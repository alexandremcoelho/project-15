from . import db
from sqlalchemy import Integer, String, Column, ForeignKey, Text

class credit_cards(db.Model):

    __tablename__="credit_cards"

    id = Column(Integer, primary_key=True)
    expire_date = Column(Text , nullable=False)
    number = Column(Text, nullable=False, unique=True)
    provider = Column(String(50), nullable=False)
    security_code = Column(String(3), nullable=False)
    user_id = Column(Integer, ForeignKey('users.id'))
    
