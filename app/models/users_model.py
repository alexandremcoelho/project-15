from . import db
from sqlalchemy import Integer, Column, Boolean, Text
from werkzeug.security import generate_password_hash, check_password_hash

class users(db.Model):

    __tablename__="users"

    id = Column(Integer, primary_key=True)
    login = Column(Text, nullable=False)
    is_admin = Column(Boolean, default=False)
    password_hash = Column(Text)

    @property
    def password(self):
        raise AttributeError("Password cannot be accessed!")

    @password.setter
    def password(self, password_to_hash):
        self.password_hash = generate_password_hash(password_to_hash)



