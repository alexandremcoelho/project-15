from flask import Flask
from flask_sqlalchemy import SQLAlchemy

db = SQLAlchemy()


def init_app(app: Flask) -> None:
    db.init_app(app)
    app.db = db

    from app.models.users_model import users
    from app.models.credit_model import credit_cards

