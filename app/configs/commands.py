from app.models.users_model import users
from app.models.credit_model import credit_cards
from flask import Flask, current_app
from flask.cli import AppGroup
from faker import Faker
from app.services.helpers import add_commit


import click


def cli_char(app: Flask):
    cli_group_char = AppGroup("user")

    @cli_group_char.command("create")
    @click.argument("registros")
    def cli_char_create(registros: int):
        for item in range(0, int(registros)):
            fake = Faker()
            char = {
                "login": fake.name()
            }
            user = users(**char)
            user.password = fake.password()
            add_commit(user)



    cli_group_adm = AppGroup("admin")
    @cli_group_adm.command("create")
    def cli_admin_create():
        fake = Faker()
        nome = fake.name()
        password = fake.password()
        obj = {
                "login": nome,
                "is_admin": True
            }
        user = users(**obj)
        user.password = password
        add_commit(user)
        click.echo(f"Admin criado!!\nlogin: {nome}\npaassword: {password}")


    app.cli.add_command(cli_group_char)
    app.cli.add_command(cli_group_adm)

def init_app(app: Flask):
    cli_char(app)